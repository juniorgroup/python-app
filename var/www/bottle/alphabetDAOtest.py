import unittest
from alphabetDAO import alphabetDAO

class alphabetDAOtest(unittest.TestCase):

    def test_getOrderedAlphabet(self):
      orderedAlphabet = alphabetDAO.getOrderedAlphabetAsList()
      self.assertEqual(orderedAlphabet,['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
                                        'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'å', 'ä', 'ö'])

    def test_getSplitOrderedAlphabetAsList(self):
      orderedAlphabet = alphabetDAO.getSplitOrderedAlphabetAsList()
      self.assertEqual(orderedAlphabet,['abcdefgh', 'ijklmnop', 'qrstuvwx', 'yzåäö']
)

if __name__== '__main__':
  unittest.main()
