import psycopg2
from textwrap import wrap

class alphabetDAO():

    def __init__(self):
      pass

    def getSplitOrderedAlphabetAsList():
      stringToSplit = (alphabetDAO.getOrderedAlphabetAsString())
      lettersPerLines = 8
      return wrap(stringToSplit,lettersPerLines)

    def getOrderedAlphabetAsString():
      conn_string = "dbname='scrambled_alphabet' user='vagrant' host='localhost' password='vagrant'"
      conn = None

      try:
        conn = psycopg2.connect(conn_string)
        cursor = conn.cursor()

        cursor.execute("SELECT * FROM LETTERS ORDER BY LETTERS;")
        records = cursor.fetchall()
        cleanedAlphabetString = [letters[0] for letters in records]
        return "".join(cleanedAlphabetString)

      finally:
        conn.close()

