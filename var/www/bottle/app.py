from bottle import Bottle, run, route, install, template, static_file
import bottle_pgsql
from alphabetDAO import *
import textwrap

app = application = Bottle()

@app.route('/')
def show_page():
  orderedSplitAlphabetList = alphabetDAO.getSplitOrderedAlphabetAsList()
  return template('templates/letters.tpl', lines =orderedSplitAlphabetList)

@app.route('static/<filename>')
def server_static(filename):
  return static_file(filename, root='var/www/bottle/static/letters.css')

if __name__ == "__main__":
  app.run(host="0.0.0.0", port=8080)
