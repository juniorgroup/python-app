

CREATE TABLE LETTERS (
    LETTER CHAR(1) PRIMARY KEY
);

INSERT INTO LETTERS(LETTER)
VALUES
  ('r'),
  ('t'),
  ('l'),
  ('g'),
  ('ö'),
  ('h'),
  ('e'),
  ('v'),
  ('w'),
  ('x'),
  ('z'),
  ('ä'),
  ('a'),
  ('i'),
  ('j'),
  ('o'),
  ('s'),
  ('d'),
  ('f'),
  ('q'),
  ('k'),
  ('m'),
  ('å'),
  ('c'),
  ('p'),
  ('n'),
  ('y'),
  ('b'),
  ('u');
